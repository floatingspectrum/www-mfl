+++
draft = false
image = "img/portfolio/tendrilrecursion.jpg"
showonlyimage = false
weight = 100
date = "2016-11-18T11:10:00+01:00"
when = "2016"
title = "Tendril Recursion"
tags = ["performance", "max-msp", "motion-sensor"]
+++
An interactive dance performance. The dancer shapes the sound in real time through her motions<!--more-->

I created a system to generate music based on a dancer’s movements. The system receives the intensity of the dancer’s gestures as motion data. It then uses the data to manipulate various sound shaping parameters.


Spektrum Berlin, Berlin, Germany<br />
April 22nd to 24th, 2016


Performance Teaser:
{{< vimeo 163556195 >}}
