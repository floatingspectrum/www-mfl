+++
date = "2016-11-18T10:51:44+01:00"
title = "About me"

+++

Thanks for visiting! My name is Mei-Fang. I am a musician and music technologist.

I enjoy using technology to craft organic and cinematic soundscapes. My music consists of ever-evolving layers of sound, blending warm and
emotive textures with noise and dissonance.

I often experiment with a generative approach. It allows me to produce rich dynamics in sounds that can't be achieved otherwise. My previous works include interactive audio systems for dance performances, visual sonification, and installations that engage people in the creation and perception of sounds. I perform music as Floating Spectrum.

I also develop sound engines for mobile and desktop apps.

![This is me][1]

[1]: /img/portfolio/scopesessions.jpg
