+++
showonlyimage = false
draft = false
image = "img/portfolio/polyphylla.jpg"
date = "2016-11-18"
when = "2014~2016"
title = "Polyphylla"
weight = 200
tags = ["software", "sound-synthesizer", "max-msp"]
+++
A fractal inspired sound synthesizer I wrote<!--more-->

I programmed a sound synthesizer that enables users to create complex tones, aided by intuitive sound visualizations. It uses fractal-inspired algorithms to make it easy to create highly dynamic and sophisticated sounds.

{{< youtube gh-kD_FLmC4 >}}


[Polyphylla Website](http://mellisonic.com)
