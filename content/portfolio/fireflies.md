+++
showonlyimage = false
draft = false
date = "2016-11-18T15:01:59+01:00"
when = "2016"
title = "Fireflies"
image = "img/portfolio/fireflies.jpg"
weight = 700
tags = ["web-audio", "multi-player", "installation"]
+++
A collaborative spatial-sound installation<!--more-->

It's part of the Lacuna [activation exhibition](https://vimeo.com/171125927) (starting at 1:59)
