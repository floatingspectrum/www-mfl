+++
showonlyimage = false
date = "2016-11-18T15:01:10+01:00"
when = "2016"
title = "forever beats"
image = "img/portfolio/foreverbeats.png"
weight = 600
tags = ["software", "synthesizer", "swift"]
+++
A commissioned project to create two custom synthesizer modules<!--more-->

Forever beats is a fractal-based music sequencer for iOS platform by John Sebastian Hussey. I designed and implemented two sound generators (an FM synth and a frequency beating drone synth) that produce rich and dynamic sounds for a fractal-based music sequencer.

[Foever Beats' official website](http://foreverbeats.audio/)
