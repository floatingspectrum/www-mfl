+++
date = "2016-11-18T15:00:01+01:00"
when = "2014"
title = "germania"
image = "img/portfolio/germania.jpg"
showonlyimage = false
weight = 900
tags = ["sound-design"]
+++
I created abstract soundscapes and sound effects to depict the dark and tense atmosphere of a theater play<!--more-->

It was shown in Akademie der Künste (Academy of Arts), Berlin, Germany
February 19th to 22nd, 2015

[Program Page](http://www.adk.de/de/programm/?we_objectID=34028)
