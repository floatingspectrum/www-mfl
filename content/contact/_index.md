+++
date = "2016-11-18T10:51:55+01:00"
title = "Get in touch"

+++

Feel free to contact me if you like my work, or if you have ideas for collaboration. I am experienced in building cutom sound patches, generative sound system, and sound design/composition.
