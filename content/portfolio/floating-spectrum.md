+++
showonlyimage = false
draft = false
image = "img/portfolio/floating-spectrum.jpg"
date = "2016-11-18"
when = "2016~now"
title = "Floating Spectrum"
weight = 50
tags = ["performance", "audio-visual"]
+++
Floating Spectrum creates organic and emotive audio/visual experiences<!--more-->

Floating Spectrum is a creative partnership between [aBe Pazos](https://hamoid.com) and me. We work with nature-inspired approaches to sound and visual creation, striving to create organic and emotive work that people feel connected to. In our  work we produce generative audio-visual systems that connect different media, in an attempt to bring new perspectives to our senses, and to reflect about the way we experience the world.

The sound material originates partially from my self-built software synthesizer, [Polyphylla](www.mellisonic.com). The material is manipulated and processed to produce various kinds of sound textures ranging from warm to dark to harsh. The multi-layered textures are like morphing clouds, subtly and constantly evolving. It intends to immerse the audiences into a rich and full universe of sound, and evoke a scope of emotions and imagery.

In addition, I created an audio analysis system to feed real-time audio data to the visual side, in order to make the motion graphics react to the sound.

Floating Spectrum's performance is a tight coupling of the visual and the aural. Subtle and complex layers of the music are visually interpreted in real time; the abstract organisms are heard as evolving shapes of Sound.


An excerpt of our performance:
{{< vimeo 327168457 >}}

Music video
{{< vimeo 363489966 >}}

## Upcoming Performances

Sweden tour December 2020 - more details soon.

## Past Performances

##### [Kiezsalon](https://www.digitalinberlin.de/kiezsalon-bendik-giske-floating-spectrum/)

Berlin, Germany, October 21st 2020

##### [Third Space](http://www.th1rdspac3.com/)


Helsinki, Finland, December 20th 2019

##### [Village Underground](https://www.villageunderground.co.uk/)


London, UK, December 14th, 2019

##### [Niviili](https://niviili.com/)


Berlin, Germany November 21st, 2019


##### [Lacuna Lab](https://lacunalab.org)


Berlin, Germany, November 14th, 2019

##### [Niu](http://www.niubcn.com/)


Barcelona Spain, April 6th 2019

##### [Taipei National University of the Arts](http://cat.tnua.edu.tw/?p=7713%3Frc%3D17)


Taipei, Taiwan, October 13th and 14th, 2016

##### [Generate the night](https://www.facebook.com/events/612156712321668/)


Taipei, Taiwan, October 1st, 2016

##### [Revolver](https://www.facebook.com/events/169624423461580/)


Taipei, Taiwan, September 31st, 2016

##### [10 Year Anniversary of Scope Session](https://www.facebook.com/events/1573616716269685/)


Panke, Berlin Germany, 24th June, 2016

##### [Generate Lab](https://festival.shedhalle.de/performances)


Shedhalle, Tübingen, Germany, May 28th, 2016

