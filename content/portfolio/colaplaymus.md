+++
image = "img/portfolio/colaplaymus-in-action.jpg"
showonlyimage = false
date = "2016-01-19T15:01:53+01:00"
when = "2016"
title = "colaplaymus"
weight = 750
tags = ["collaborative", "spatial", "performance"]
+++
Multi-laptop spatial sound composition<!--more-->

Together with [aBe Pazos](https://hamoid.com), I created a multi-laptop, spatial sound composition. The composition is realized by the participants: dierent parts of the score are randomly assigned to the participants to perform together. The participants follows the virtual conductor's instructions to create sounds using their devices. The collective effort turns into a spatial sound composition.

The work was part of the [Transmediale Vorspiel 2016]( http://spektrumberlin.de/events/detail/scs-showcase.html)
